// problem1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <string>
#include <iomanip>

using namespace std;

double BGN_USD = 0;
double BGN_EUR = 0;
double USD_EUR = 0;
double USD_BGN = 0;
double EUR_BGN = 0;
double EUR_USD = 0;

string c1, c2;
double rate;
double value;

double sum = 0;
const int EXIT = -1;

int ex_set(string a, string b, double c)
{
	cin >> a >> b >> c;


	if (a == "BGN" && b == "USD")
	{
		BGN_USD = c;
	}
	else if (a == "BGN" && b == "EUR")
	{
		BGN_EUR = c;
	}
	else if (a == "USD" && b == "EUR")
	{
		USD_EUR = c;
	}
	else if (a == "USD" && b == "BGN")
	{
		USD_BGN = c;
	}
	else if (a == "EUR" && b == "BGN")
	{
		EUR_BGN = c;
	}
	else if (a == "EUR" && b == "USD")
	{
		EUR_USD = c;
	}
	else
	{
		cout << "N.A" << endl;
	}
	if (c == 0)
	{
		cout << "N.A" << endl;
	}
	return 0;
}

int execute_convert(string a, string b, double c)
{
	cout << fixed << setprecision(2);
	cin >> a >> b >> c;
	double result = 0;

	if (c1 == "BGN" && c2 == "USD")
	{
		result = c * BGN_USD;
	}
	else if (c1 == "BGN" && c2 == "EUR")
	{
		result = c * BGN_EUR;
	}
	else if (c1 == "USD" && c2 == "EUR")
	{
		result = c * USD_EUR;
	}
	else if (c1 == "USD" && c2 == "BGN")
	{
		result = c * USD_BGN;
	}
	else if (c1 == "EUR" && c2 == "BGN")
	{
		result = c * EUR_BGN;
	}
	else if (c1 == "EUR" && c2 == "USD")
	{
		result = c * EUR_USD;
	}

	cout << result << endl;
	return 0;
}


int execute_rate(string a, string b)
{
	cout << fixed << setprecision(6);
	cin >> a >> b;


	if (a == "BGN" && b == "USD")
	{
		cout << BGN_USD << endl;
	}
	else if (a == "USD" && b == "EUR")
	{
		cout << USD_EUR << endl;
	}
	return 0;
}
int exit()
{
	cout << "Goodbye." << endl;
	return EXIT;
}

int executee(const string &cmd)
{
	if (cmd == "set")
	{
		return ex_set(c1, c2, rate);
	}
	else if (cmd == "convert")
	{
		return execute_convert(c1, c2, value);
	}
	else if (cmd == "rate")
	{
		return execute_rate(c1, c2);
	}
	else if(cmd == "exit")
	{
		return exit();
	}
	else
	{
		cout << "Unrecognizable command." << endl;
	}
}

int main()
{
	cout << "Enter command: " << endl;
	string cmd;

	while (cin >> cmd)
	{
		int finalCode = executee(cmd);
		if (finalCode == EXIT)
		{
			break;
		}
	}

	return 0;
}


