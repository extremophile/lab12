// problem2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>	
#include <string> 

using namespace std;

char one = 'I';
char five = 'V';
char ten = 'X';



void transform(int a)
{
	string foo;
	if (a >= 1 && a < 4)
	{
		foo = string(a, one);
		cout << foo << endl;
	}
	if (a == 4)
	{
		cout << "IV" << endl;
	}
	if (a == 5)
	{
		cout << five << endl;
	}
	if (a >= 6 && a < 9)
	{
		foo = string(a - 5, one);
		cout << five << foo << endl;
	}
	if (a == 9)
	{
		cout << "IX" << endl;
	}
	if (a == 10)
	{
		cout << "X" << endl;
	}
	if (a > 10 && a < 14)
	{
		foo = string(a - 10, one);
		cout << "X" << foo << endl;
	}
	if (a == 14)
	{
		cout << "XIV" << endl;
	}
	if (a == 15)
	{
		cout << "XV" << endl;
	}

}

int main()
{
	int n;
	cout << "Enter a number: " << endl;
	while (cin >> n)
	{
		transform(n);
	}

	return 0;
}

